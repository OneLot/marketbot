#!/bin/bash

X_WITH="X-Requested-With: XMLHttpRequest"
UA="User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36"

curl -H "${X_WITH}" -H "${UA}" \
"https://www.investing.com/indices/Service/SpecificationsFutures?pairid=0&sid=785570e03c27af532af0f655f38af&filterParams=&smlID=70" \
> index_futures.resp

curl -H "${X_WITH}" -H "${UA}" \
"https://www.investing.com/commodities/Service/Specifications?pairid=0&sid=7a3b58eb7cfb629ec9b32841c86e0360&filterParams=&smlID=182" \
> commodity_futures.resp

curl -H "${X_WITH}" -H "${UA}" \
"https://www.investing.com/ratesbonds/Service/SpecificationsFutures?pairid=0&sid=4198194e499a8f72007436da623be59b&filterParams=&smlID=200000" \
> financial_futures.resp

curl -H "${X_WITH}" -H "${UA}" \
"https://www.investing.com/equities/adv-micro-device" \
> amd.resp

curl -H "${X_WITH}" -H "${UA}" \
"https://www.investing.com/equities/adv-micro-device" \
> amd_pm.resp

curl -H "${X_WITH}" -H "${UA}" \
"https://www.investing.com/indices/us-spx-500-futures" \
> es.resp

curl -H "${X_WITH}" -H "${UA}" \
-H "Content-Type: application/x-www-form-urlencoded" \
-d 'search_text=amd&term=amd&country_id=0&tab_id=All' \
"https://www.investing.com/search/service/search" \
> amd_search.resp

curl -H "${X_WITH}" -H "${UA}" -H "Referer: https://www.investing.com" \
"https://ssltvc.forexprostools.com/87ec66a24d0f0f35df500ff41071333e/1529883800/1/1/7/history?symbol=8274&resolution=15&from=1529883800&to=1532476018" > amd_data.resp


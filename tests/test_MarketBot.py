import io
import os
import unittest
from typing import List
import requests
import requests_mock
from functional import seq
from marketbot import marketbot
import mock
import re
import asynctest
from discord.ext import commands
import parameterized
import gyazo
import pandas
import json
import datetime
import pyinvesting


class MockInvesting(object):
    def __init__(self):
        directory_path = os.path.dirname(__file__)
        self.fixture_path = os.path.join(directory_path, 'fixtures/')
        self.mock_symbol = False
        self.mock_bonds = False

    def reset_symbol(self):
        self.mock_symbol = False

    def get_events_by_date(self, date_from, date_to=None):
        mock.sentinel.event_one.update_time = mock.sentinel.update_time_one
        mock.sentinel.event_two.update_time = mock.sentinel.update_time_two
        return [mock.sentinel.event_one, mock.sentinel.event_two]

    def get_symbol(self, symbol='amd'):
        if self.mock_symbol:
            return self.mock_symbol

        mock_symbol = mock.Mock()

        try:
            symbol_data = json.load(
                open(self.fixture_path + '/%s_data.json' % (symbol)))
        except FileNotFoundError:
            print("Missing mock data for %s.  Returning default AMD stub")
            symbol_data = json.load(
                open(self.fixture_path + '/%s_data.json' % ('amd')))

        mock_symbol.name = symbol_data['name']
        mock_symbol.info = symbol_data['info']
        mock_symbol.url = symbol_data['url']
        mock_symbol.quote.return_value = symbol_data['quote']
        mock_symbol.overview.return_value = symbol_data['overview']

        # Options data
        try:
            options_df = pandas.read_json(
                open(self.fixture_path +
                     '/%s_data_options.json' % symbol).read())
            mock_symbol.get_options.return_value = (options_df.copy(),
                                                    1538097009)

        except FileNotFoundError:
            mock_symbol.get_options.return_value = None

        # Chart data
        try:
            data_series = open(
                self.fixture_path + '%s_data_chart.json' % symbol,
                'rb').read()
            mock_symbol.get_data_series.side_effect = [
                data_series,
                '{"s":"no_data","nextTime":1532720700}'.encode('utf-8'),
                data_series,
            ]

        except FileNotFoundError:
            mock_symbol.get_data_series.return_value = None

        self.mock_symbol = mock_symbol
        return mock_symbol

    def get_bonds(self, region):
        try:
            bond_data = json.load(
                open(self.fixture_path + '/{0}_bonds.json'.format(region)))
            return (seq(bond_data).map(
                lambda mbd: pyinvesting.Bond(mbd["yield_pct"], mbd["duration_label"], mbd["duration_months"], region, "bond-mock.com")
            ).to_list())
        except FileNotFoundError:
            return None


class TestMarketBot(asynctest.TestCase):
    def setUp(self):
        self.mock_bot = mock.Mock()
        self.mock_bot.send_message = asynctest.CoroutineMock()
        self.mock_gyazo = mock.Mock()
        self.mock_scheduler = mock.Mock()
        mock_investing = MockInvesting()
        self.fixture_path = mock_investing.fixture_path
        self.marketbot = marketbot.MarketBot(
            self.mock_bot,
            investing=mock_investing,
            gyazo=self.mock_gyazo,
            scheduler=self.mock_scheduler,
            bot_event_channels=[mock.sentinel.event_channel])

        self.marketbot.bot.user = 'Me'
        self.marketbot.bot.process_commands = asynctest.CoroutineMock()
        self.marketbot.bot.command_prefix = '&'
        self.marketbot.bot.logger.exception.side_effect = print

        ctx = mock.Mock()
        ctx.message.channel.name = "testchannel"
        ctx.message.author = "someguy"
        ctx.bot.say = asynctest.CoroutineMock()
        ctx.bot.type = asynctest.CoroutineMock()
        self.ctx = ctx

    async def test_schedule_days_events(self):
        self.mock_scheduler.reset_mock()
        await self.marketbot.schedule_days_events()
        self.assertEquals(self.mock_scheduler.add_job.call_count, 2)
        scheduler_calls = self.mock_scheduler.add_job.mock_calls
        for i, event in enumerate(
                self.marketbot.investing.get_events_by_date(
                    date_from=datetime.date.today())):
            # check scheduled callback
            self.assertEquals(scheduler_calls[i][2]['args'], [event])
            # check scheduled date
            self.assertEquals(scheduler_calls[i][2]['run_date'],
                              event.update_time)

    async def test_get_event_result(self):
        # Transition from unreleased to released
        mock_event = mock.Mock(
            actual=None,
            name='Mock Event',
            retrieved_time=datetime.datetime.now())
        mock_event.get.side_effect = [None, None, mock.sentinel.result]
        result = await self.marketbot.get_event_result(
            mock_event, interval=0.1, max_wait=10)
        self.assertEqual(result, mock.sentinel.result)

        # Already released data
        mock_event = mock.Mock(
            actual=mock.sentinel.result,
            name='Mock Event',
            retrieved_time=datetime.datetime.now())
        mock_event.get.side_effect = [mock.sentinel.result]
        result = await self.marketbot.get_event_result(
            mock_event, interval=0.1, max_wait=10)
        self.assertEqual(result, mock.sentinel.result)

    async def test_announce_event(self):
        # test failure
        mock_event = mock.Mock()
        mock_event.get.return_value = None
        await self.marketbot.announce_event(
            mock_event, fetch_interval=1, fetch_max_wait=1)
        self.mock_bot.send_message.assert_not_called()

        # test success
        event = pyinvesting.Event(
            name='Mock Event',
            url='/eventurl',
            client=None,
            event_id=mock.sentinel.event_id,
            actual=mock.sentinel.result,
            previous=mock.sentinel.previous,
            expected=mock.sentinel.expected,
            retrieved_time=datetime.datetime.now(),
        )

        mock_server = mock.Mock()
        self.mock_bot.connection.servers = [mock_server]

        mock_server.get_channel.return_value = mock.sentinel.event_channel
        await self.marketbot.announce_event(
            event, fetch_interval=1, fetch_max_wait=1)

        mock_server.get_channel.return_value = None
        await self.marketbot.announce_event(
            event, fetch_interval=1, fetch_max_wait=1)

        self.mock_bot.send_message.assert_called_once_with(
            mock.sentinel.event_channel, embed=mock.ANY)

    async def test_get_data_embed_for_event(self):
        event = pyinvesting.Event(
            name='Mock Event',
            url='/eventurl',
            client=None,
            event_id=mock.sentinel.event_id,
            actual=mock.sentinel.result,
            previous=mock.sentinel.previous,
            expected=mock.sentinel.expected,
            retrieved_time=datetime.datetime.now(),
        )
        e = await self.marketbot.get_data_embed_for_event(
            mock.sentinel.previous, event)
        self.assertEqual(e.title, event.name)
        self.assertRegexpMatches(e.url, '.*\/eventurl$')
        embed_dict = e.to_dict()
        self.assertEqual(embed_dict['fields'][0], {
            'inline': True,
            'name': 'Actual',
            'value': str(event.actual),
        })
        self.assertEqual(embed_dict['fields'][1], {
            'inline': True,
            'name': 'Expected',
            'value': str(event.expected),
        })
        self.assertEqual(embed_dict['fields'][2], {
            'inline': True,
            'name': 'Previous',
            'value': str(event.previous),
        })
        self.assertNotIn(
            'footer',
            embed_dict.keys(),
            msg="Attached footer despite no revision")

        revised_e = await self.marketbot.get_data_embed_for_event(
            mock.sentinel.different_previous, event)
        revised_embed_dict = revised_e.to_dict()

        self.assertIn('footer', revised_embed_dict.keys())
        self.assertRegexpMatches(revised_embed_dict['footer']['text'],
                                 '^previous release was revised.*')

    async def test_on_message_me(self):
        message = mock.Mock()
        message.content = "!blah"
        message.author.return_value = 'Me'

        await self.marketbot.on_message(message)
        self.marketbot.bot.process_commands.assert_not_called()

    async def test_on_message(self):
        quote_list = [
            ('foo $tsla foo', 'tsla'),
            ('$t', 't'),
            ('$t1', 't1'),
            ('$t+', 't+'),
            ('$nq+', 'nq+'),
            ('$cbot:zw', 'cbot:zw'),
            ('$cbot:zw+', 'cbot:zw+'),
            ('$usd/jpy', 'usd/jpy'),
            ('$usd/jpy+', 'usd/jpy+'),
            ('$t foo $amd foo $es+ foo $cme:es $cbot:zw+',
             't,amd,es+,cme:es,cbot:zw+'),
        ]

        ignore_list = [
            '$1',
            'foo $1 foo',
            '$1k',
            'foo $1k foo',
        ]

        for (msg, symbol) in quote_list:
            with self.subTest(msg=msg):
                message = mock.Mock()
                message.content = msg
                quote_command = '&quote %s' % symbol
                await self.marketbot.on_message(message)
                self.marketbot.bot.process_commands.assert_awaited_once()
                self.assertEqual(message.content, quote_command)
                self.marketbot.bot.process_commands.reset_mock()

        for msg in ignore_list:
            with self.subTest(message=msg):
                message = mock.Mock()
                message.content = msg
                await self.marketbot.on_message(message)
                self.marketbot.bot.process_commands.assert_not_called()

    @parameterized.parameterized.expand([
        ('amd', ['amd'], False),
        ('amd+', ['amd'], True),
        ('t', ['t'], False),
        ('t+', ['t'], True),
        ('cme:es', ['cme:es'], False),
        ('cbot:zw+', ['cbot:zw'], True),
        ('usd/jpy', ['usd/jpy'], False),
        ('usd/jpy+', ['usd/jpy'], True),
    ])
    async def test_quote(self, symbol, embed_symbol, embed_overview):

        await self.marketbot.quote.callback(
            self=self.marketbot, ctx=self.ctx, symbol=symbol)  # pylint: disable=E1101

        control_embed = await self.marketbot.get_data_embed_for_symbols(
            embed_symbol, overview=embed_overview)

        self.ctx.bot.say.assert_called_with(
            embed=mock.ANY, delete_after=mock.ANY)

        # Make sure embed looks right
        sent_embed = self.ctx.bot.say.mock_calls[0][2]['embed']

        self.assertEquals(control_embed.to_dict(), sent_embed.to_dict())
        self.ctx.bot.say.reset_mock()
        self.ctx.bot.type.assert_called()

    async def test_mock_data_embed(self):

        mock_symbol = MockInvesting().get_symbol('amd')
        embed = await self.marketbot.get_data_embed_for_symbols(['amd'])

        self.assertEqual(embed.title, mock_symbol.info)
        self.assertEqual(embed.url, mock_symbol.url)
        self.assertEqual(embed.fields[0].name, 'AMD')
        self.assertEqual(
            embed.fields[0].value,
            '%(last)s -> (%(changed)s / %(changed_pct)s)' %
            mock_symbol.quote())
        mock_symbol.quote.assert_called()

        # Test AH
        self.marketbot.investing.reset_symbol()
        mock_symbol = MockInvesting().get_symbol('amd-ah')
        embed = await self.marketbot.get_data_embed_for_symbols(['amd-ah'])
        self.assertEqual(
            embed.fields[0].value,
            '%(last)s -> (%(changed)s / %(changed_pct)s) | AH: %(ah_last)s -> (%(ah_changed)s / %(ah_changed_pct)s)'
            % mock_symbol.quote())

        # Test PM
        self.marketbot.investing.reset_symbol()
        mock_symbol = MockInvesting().get_symbol('amd-pm')
        embed = await self.marketbot.get_data_embed_for_symbols(['amd-pm'])
        self.assertEqual(
            embed.fields[0].value,
            '%(last)s -> (%(changed)s / %(changed_pct)s) | PM: %(pm_last)s -> (%(pm_changed)s / %(pm_changed_pct)s)'
            % mock_symbol.quote())

        # Test Overview Data
        self.marketbot.investing.reset_symbol()
        mock_symbol = MockInvesting().get_symbol('amd')
        embed = await self.marketbot.get_data_embed_for_symbols(['amd'],
                                                                overview=True)
        self.assertEqual(embed.fields[0].name, 'AMD')
        # noinspection PyStringFormat,PyStringFormat
        self.assertEquals(
            embed.fields[0].value,
            '%(last)s -> (%(changed)s / %(changed_pct)s)\n\nOpen: %(Open)s | Volume: %(Volume)s'
            % {
                **mock_symbol.quote(),
                **mock_symbol.overview()
            })

    async def test_get_chart(self):
        # mock_symbol = mock.Mock()
        # mock_symbol.info = 'Advanced Micro Devices Inc'
        # data = open('tests/fixtures/amd_data.resp', 'rb').read()
        # mock_symbol.get_data_series.return_value = data
        # self.marketbot.investing.get_symbol.return_value = mock_symbol
        chart_buffer = await self.marketbot.get_chart('amd', resolution='30')
        self.assertIsInstance(chart_buffer, io.BytesIO)
        open('/tmp/test_chart_output.png',
             'wb').write(chart_buffer.getbuffer())

        # mock_symbol.get_data_series.side_effect = [
        #     '{"s":"no_data","nextTime":1532720700}'.encode('utf-8'),
        #     data,
        # ]
        chart_buffer = await self.marketbot.get_chart('amd', resolution='30')

        mock_symbol = self.marketbot.investing.mock_symbol
        mock_symbol.get_data_series.assert_called_with(
            resolution='30', to_time=1532720700)
        self.assertIsInstance(chart_buffer, io.BytesIO)

    @parameterized.parameterized.expand([
        ('amd', 'fd1', 'amd_data_options.json'),
        ('es', 'fd1', False),
        (False, 'fd1', False),
    ])
    async def test_options(self, symbol, expiration, pd_json):
        # self.setUp()

        if pd_json:
            options_data = json.load(open(self.fixture_path + '/%s' % pd_json))
        else:
            options_data = None

        await self.marketbot.options.callback(
            self=self.marketbot,
            ctx=self.ctx,
            symbol=symbol,
            expiration=expiration)  # pylint: disable=E1101

        mock_symbol = self.marketbot.investing.mock_symbol

        self.ctx.bot.type.assert_called()

        # Symbol is good but no options
        if symbol and not pd_json:
            mock_symbol.get_options.assert_called()
            self.ctx.bot.say.assert_not_called()

        # Symbol is good and options are present
        elif symbol and pd_json:
            # This should work but the options call path is fucked and
            # pulls the symbol twice
            # mock_symbol.get_options.assert_called()
            self.ctx.bot.say.assert_called()

            # This should be in another test but it's slightly easier to re-use the existing mock setup here
            (options_table, _) = await self.marketbot.get_options_data(
                self.marketbot.investing.mock_symbol, expiration)
            for field in [
                    'call_bid', 'call_ask', 'strike', 'put_bid', 'put_ask'
            ]:
                self.assertRegexpMatches(options_table.getvalue(),
                                         str(options_data[0][field]))

        # Symbol is no good
        else:
            self.ctx.bot.say.assert_not_called()

    @parameterized.parameterized.expand([
        ('us', 'us_bonds.json'),
        ('usa', False),
    ])
    async def test_get_bonds_table_data(self, region, bonds_json):
        if bonds_json:
            bonds = self.marketbot.investing.get_bonds('us')
        else:
            bonds = None

        if bonds is not None:
            expected_output = '```  Mat   Yield\n----- -------\n1M    +2.46  \n3M    +2.44  \n6M    +2.51  \n1Y    +2.52  \n2Y    +2.44  \n3Y    +2.39  \n5Y    +2.40  \n7Y    +2.49  \n10Y   +2.59  \n30Y   +3.01  \n```'
            (bonds_table, _,
             _) = await self.marketbot.get_bonds_table_data(region)
            self.assertEquals(expected_output, bonds_table.getvalue())
        else:
            self.ctx.bot.say.assert_not_called()

    async def test_bonds(self):
        await self.marketbot.bonds.callback(
            self=self.marketbot, ctx=self.ctx, region='us')

        self.ctx.bot.type.assert_called()
        sent_embed = self.ctx.bot.say.mock_calls[0][2]['embed']
        expeted_embed_dict = {
            'fields': [{
                'inline':
                True,
                'name':
                'Bonds Term Structure',
                'value':
                '```  Mat   Yield\n----- -------\n1M    +2.46  \n3M    +2.44  \n6M    +2.51  \n1Y    +2.52  \n2Y    +2.44  \n3Y    +2.39  \n5Y    +2.40  \n7Y    +2.49  \n10Y   +2.59  \n30Y   +3.01  \n```'
            }],
            'color':
            16777215,
            'type':
            'rich',
            'url':
            'bond-mock.com',
            'title':
            'us Sovereign Debt'
        }
        self.assertEqual(sent_embed.to_dict(), expeted_embed_dict)

        await self.marketbot.bonds.callback(
            self=self.marketbot, ctx=self.ctx, region='fail')
        self.ctx.bot.say.assert_called_once()

    def test_upload_chart(self):
        mock_image = mock.Mock()

        mock_image.url = 'https://i.gyazo.com/7b985a6d1999c2bc8fd2fefc3ee613f3.png'

        self.mock_gyazo.upload_image.return_value = mock_image
        url = self.marketbot.upload_chart(mock.sentinel.chart_buffer)
        self.mock_gyazo.upload_image.assert_called_with(
            mock.sentinel.chart_buffer)

        self.assertEqual(mock_image.url, url)

    async def test_chart(self):
        await self.marketbot.chart.callback(
            self=self.marketbot, ctx=self.ctx, symbol='amd', resolution='30')  # pylint: disable=E1101

        self.ctx.bot.say.assert_called()
        self.ctx.bot.type.assert_called()

        # Make sure empty config doesn't try to chart
        self.marketbot.gyazo = None
        await self.marketbot.chart.callback(
            self=self.marketbot, ctx=self.ctx, symbol='amd', resolution='30')  # pylint: disable=E1101

        self.ctx.bot.say.assert_called_once()
        self.ctx.bot.type.assert_called_once()

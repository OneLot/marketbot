## Installing

The bot can be built with the dockerfile (recommended) or install with pip:

```
pip install -r requirements.txt
pip install --upgrade .
```

## Running

In order to run the bot you must have a discord bot token.  Additionally, you may acquire a gyazo access token to upload charts. 

### Configuration
marketbot takes configuration from ENV variables or any other format supported by [dynaconf](https://dynaconf.readthedocs.io/en/latest/).  ENV variables are recommended and better tested but using a settings file configured correctly should work.

If using a settings file, define a 'marketbot' environment and remove the MARKETBOT prefix to any values.

MARKETBOT_COMMAND_PREFIX: the command prefix for the bot

MARKETBOT_DISCORD_TOKEN: discord bot token

MARKETBOT_GYAZO_TOKEN: gyazo api token.  Optional.

MARKETBOT_EVENT_CHANNELS: list of numeric channel ids to announce market events to.  Optional.

### Command line
```
MARKETBOT_DISCORD_TOKEN=[blah] MARKETBOT_GYAZO_TOKEN=[foo] [etc] marketbot
```

### Docker
```
docker run -it \
  -e MARKETBOT_DISCORD_TOKEN=[BLAH] \
  -e MARKETBOT_GYAZO_TOKEN=[FOO] \
  -e [other flag]=[other setting] \
  marketbot
```
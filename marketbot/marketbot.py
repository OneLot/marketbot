#!/usr/bin/env python

import io
import re
import os
import json
import discord
import pytz
from discord.ext import commands
import time
import hashlib
import pandas
import datetime
import numpy as np
import gyazo
import pyinvesting
import astropy.io.ascii
import asyncio
import functools
import typing
from typing import Optional, List, Tuple, Dict
from pyinvesting import Constituent, Index
from pyinvesting.event import Event
from pyinvesting.bond import Bond
from discord.ext import commands
from functional import seq
from dateutil import relativedelta
import matplotlib

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.pyplot as plt
from mpl_finance import candlestick_ohlc


# matplotlib.use('Cairo')


class EmbedData(object):
    def __init__(self, info=None, url=None, symbol=None, text=None):
        self.info = info
        self.url = url
        self.symbol = symbol
        self.text = text


class MarketBot(object):
    def __init__(
            self,
            bot,
            investing=None,
            gyazo=None,
            scheduler=None,
            event_debug=False,
            bot_event_channels: List[str] = None,
            indexman_base: str = None
    ):
        if investing == None:
            self.investing: pyinvesting.Client = pyinvesting.Client(indexman_base)
        else:
            self.investing: pyinvesting.Client = investing

        self.gyazo = gyazo
        self.bot = bot
        self.bot_event_channels = bot_event_channels
        self.symbol_cooldown = {}
        self.symbol_cooldown_length = 30
        self.bot.logger.debug(self.bot.commands)
        self.event_loop = asyncio.get_event_loop()
        self.scheduler = scheduler
        self.event_loop.create_task(self.schedule_days_events())
        self.event_loop.create_task(self.schedule_index_close_recap())

        # Allow the bot to retroactively dump everything from today for testing
        if event_debug:
            misfire_grace_time = 86400
            self.event_loop.call_later(
                30, lambda: asyncio.ensure_future(self.schedule_days_events(misfire_grace_time=misfire_grace_time)))

        self.scheduler.add_job(
            self.schedule_days_events,
            'cron',
            hour='0',
            minute='1',
            second='1',
            name='Daily Event Refresh')

        # self.scheduler.add_job(
        #
        # )

        self.bot.logger.info("Finished initializing Marketbot")

    async def schedule_days_events(self, misfire_grace_time=60):
        """Schedules list of Event objects to be retrieved later
        """
        self.bot.logger.info('Refreshing economic calendar')
        events = self.investing.get_events_by_date(
            date_from=datetime.date.today(), date_to=datetime.date.today())
        self.bot.logger.info("Retrieved " + str(len(events)) +
                             " events from investing.com post end point.")
        for event in events:
            self.bot.logger.info("Scheduling event '%s' at %s" %
                                 (event.name, str(event.update_time)))
            self.scheduler.add_job(
                self.announce_event,
                'date',
                run_date=event.update_time,
                args=[event],
                misfire_grace_time=misfire_grace_time)

    async def schedule_index_close_recap(self, misfire_grace_time=60):
        """Schedules lunch and index recap using yesterday's index compo"""
        self.bot.logger.info('Scheduling index recaps')
        spx = self.investing.get_spx_index()
        # announcement_batches = [(16, 30), (20, 30)]          # 12:30 / 4:30 EST
        announcement_batches = [(13, 31), (13, 45), (13, 55),  # 9:31 EST / 9:45 EST / 9:55 EST
                                (14, 3), (18, 45), (20, 0)]   # 10:30 EST / 2:45 EST / 4:00 EST
        for hour_minute in announcement_batches:
            self.scheduler.add_job(
                self.announce_index_movers,
                'date',
                run_date=datetime.datetime.today().replace(hour=hour_minute[0], minute=hour_minute[1], tzinfo=pytz.UTC),
                args=[spx],
                misfire_grace_time=misfire_grace_time
            )

    async def get_event_result(self, event, interval=5,
                               max_wait=120) -> Optional[str]:
        """
        Attempts to fetch an event until a result is  published
        This method will block until the result or max_wait has elapsed
        Returns | event result or None if we time out
        """
        self.bot.logger.info(
            "Starting to fetch results for '%s' for %s ms at %s ms intervals" %
            (event.name, max_wait, interval))
        start_time = time.time()
        deadline_time = start_time + max_wait

        while time.time() < deadline_time:
            result = event.get()
            if result is None:
                self.bot.logger.warning(
                    ("Did not get new release for {0} yet".format(event.name)))
                await asyncio.sleep(interval)
            else:
                self.bot.logger.info("New %s release, at %s" %
                                     (event.name, event.retrieved_time))
                return result
        return None

    async def announce_event(self,
                             event: pyinvesting.Event,
                             fetch_interval=5,
                             fetch_max_wait=300):
        """
        Retrieves event actual and changes previous if last release is revised,
        format it and announce it if released
        """
        previous_release = event.previous
        result = await self.get_event_result(
            event, interval=fetch_interval, max_wait=fetch_max_wait)
        if result is not None:
            embed_msg = await self.get_data_embed_for_event(
                previous_release, event)
            for server in self.bot.connection.servers:
                self.bot.logger.info(
                    "Announcing %s to server %s" % (event.name, server))
                for channel_id in self.bot_event_channels:
                    channel = server.get_channel(channel_id)
                    if channel is not None:
                        self.bot.logger.info("Announcing %s to channel %s" %
                                             (event.name, channel_id))
                        await self.bot.send_message(channel, embed=embed_msg)

    @staticmethod
    async def get_index_mover_table(constituents: List[Constituent]) -> io.StringIO:
        ticker_poi_weight_t_list: List[Tuple[str, float, float]] = (
            [(c.ticker, round(c.point_of_index_daily_move, 3), round(c.weight, 2)) for c in constituents]
        )
        col1 = [c[0] for c in ticker_poi_weight_t_list]
        col2 = [c[1] for c in ticker_poi_weight_t_list]
        col3 = [c[2] for c in ticker_poi_weight_t_list]
        column_names = ['Ticker', 'PointIndex', 'Weight']
        table = io.StringIO()
        table.write("```")
        astropy.io.ascii.write(
            [col1, col2, col3],
            table,
            format='fixed_width_two_line',
            formats={'Ticker': '%-6s', 'PointIndex': '%-10s', 'Weight': '%-10s'},
            names=column_names
        )
        table.write("```")
        return table

    async def announce_index_movers(self,
                                    index: Index):
        index.refresh_prices()
        spx_idx: Index = index.get_index_px()
        if bool(spx_idx):
            self.bot.logger.info(
                "Retrieved spx with future {0} cash {1} basis {2}".format(spx_idx['future'], spx_idx['cash'],
                                                                          spx_idx['basis']))
        else:
            self.bot.logger.warning("Cannot announce movers without PX connection")
            return
        (top_5,
         bot_5) = index.get_movers(n=5)
        (top_5_table, bot_5_table) = (await self.get_index_mover_table(top_5),
                                      await self.get_index_mover_table(bot_5))

        top_5_msg = await self.get_data_embed_for_index_movers(title="Top 5 SPX Movers")
        bot_5_msg = await self.get_data_embed_for_index_movers(title="Bot 5 SPX Movers")
        top_5_msg.add_field(
            name="Descending index impact",
            value=top_5_table.getvalue(),
            inline=True)
        bot_5_msg.add_field(
            name="Ascending index impact",
            value=bot_5_table.getvalue(),
            inline=True
        )

        for server in self.bot.connection.servers:
            self.bot.logger.info("Announcing movers to server %s" % server)
            for channel_id in self.bot_event_channels:
                channel = server.get_channel(channel_id)
                if channel is not None:
                    self.bot.logger.info("Announcing movers to channel " + str(channel_id))
                    await self.bot.send_message(channel, embed=top_5_msg)
                    await self.bot.send_message(channel, embed=bot_5_msg)

    async def get_chart(self, symbol, resolution='15', bars=30):
        s = await self.event_loop.run_in_executor(
            None, self.investing.get_symbol, symbol)
        data = await self.event_loop.run_in_executor(
            None, functools.partial(
                s.get_data_series,
                resolution=resolution,
            ))
        j_data = json.loads(data.decode('utf-8'))

        # If we get no data, we may have been told to go back in time
        if j_data['s'] == 'no_data':
            data = await self.event_loop.run_in_executor(
                None,
                functools.partial(
                    s.get_data_series,
                    resolution=resolution,
                    to_time=j_data['nextTime'],
                ))
        df = pandas.read_json(data)

        trimmed_df = df.tail(bars).copy()
        trimmed_df.reset_index(inplace=True)

        # Convert to actual timestamps
        dates = pandas.to_datetime(trimmed_df['t'], unit='s')

        # Extract ohlc in correct order
        ohlc_values = trimmed_df[['o', 'h', 'l', 'c']]

        fig = plt.figure(figsize=(25, 15))
        fig.set_facecolor((0, 0, 0, 0))
        ax1 = plt.subplot2grid((1, 1), (0, 0))

        candlestick_ohlc(
            ax1,
            ohlc_values.itertuples(),
            width=0.75,
            colorup='#0984e3',
            colordown='#d63031',
            alpha=0.85,
        )

        def format_date(x, pos=None) -> io:
            thisind = np.clip(int(x + 0.5), 0, len(dates) - 1)

            if resolution == 'D' or resolution == 'W':
                time_string = '%m/%d'
            elif resolution == 'M':
                time_string = '%Y/%m'
            else:
                time_string = '%d %H:%M'
            return dates[thisind].strftime(time_string)

        ax1.xaxis.set_major_formatter(mticker.FuncFormatter(format_date))
        ax1.xaxis.set_major_locator(mticker.MaxNLocator(5))
        ax1.yaxis.set_major_locator(mticker.MaxNLocator(5))
        # ax1.grid(True, color='#C2C2C2',linewidth=2)
        ax1.grid(False)
        ax1.set_facecolor((0, 0, 0, 0))
        ax1.tick_params(colors='#C2C2C2', labelsize=35)
        # Fix vertical line thickness
        for line in ax1.lines:
            line.set_linewidth(3)
            line.set_alpha(0.85)

        fig.autofmt_xdate()

        out_buffer = io.BytesIO()
        plt.savefig(out_buffer, transparent=True, format='png')
        plt.close()
        return out_buffer

    def upload_chart(self, chart_buffer) -> str:
        image = self.gyazo.upload_image(chart_buffer)
        return image.url

    def get_embed(self):
        e = discord.Embed()
        # noinspection PyDunderSlots,PyUnresolvedReferences
        e.color = 16777215  # pylint: disable=E0237
        return e

    async def get_embed_field(self, symbol, overview=False):
        whitelisted_keys = [
            "Day's Range",
            "52 wk Range",
            "Next Earnings Date",
            "Volume",
            "Open",
            "Prev. Close",
            "Tick Value",
            "Month",
            "Tick Size",
        ]
        s = await self.event_loop.run_in_executor(
            None, self.investing.get_symbol, symbol)
        try:

            if s:
                q = await self.event_loop.run_in_executor(None, s.quote)
                if overview:
                    o = await self.event_loop.run_in_executor(None, s.overview)
            else:
                self.bot.logger.info("Symbol %(symbol)s not found" % {'symbol': symbol})
                return

        except Exception as e:
            self.bot.logger.exception(
                "Failed getting quote for %(symbol)s: %(exception)s" % {'symbol': symbol, 'exception': e})
            return

        f = EmbedData()
        f.info = s.info
        f.url = s.url
        f.symbol = symbol

        # Build message for normal, pre market or after hours

        quote_message = "%(last)s -> (%(changed)s / %(changed_pct)s)" % q
        if 'ah_last' in q.keys():
            quote_message += " | AH: %(ah_last)s -> (%(ah_changed)s / %(ah_changed_pct)s)" % q
        elif 'pm_last' in q.keys():
            quote_message += " | PM: %(pm_last)s -> (%(pm_changed)s / %(pm_changed_pct)s)" % q

        if overview:
            overview = []
            for field in sorted(o.keys()):
                if field in whitelisted_keys:
                    overview.append('%(field)s: %(data)s' % {
                        'field': field,
                        'data': o[field]
                    })
            overview_message = ' | '.join(overview)
            quote_message += '\n\n%s' % overview_message

        f.text = quote_message
        return f

    async def get_data_embed_for_symbols(self, symbols, overview=False):
        e = self.get_embed()

        for symbol in symbols[:3]:
            f = await self.get_embed_field(symbol, overview)
            if f:
                e.add_field(name=symbol.upper(), value=f.text)

        if f:
            if len(symbols) == 1:
                e.title = f.info
                e.url = f.url
            return e

    async def get_data_embed_for_bonds(self, region: str, url: str):
        e = self.get_embed()
        e.title = region + " Sovereign Debt"
        e.url = url
        self.bot.logger.info(e.url)

        return e

    async def get_data_embed_for_index_movers(self, title: str):
        e = self.get_embed()
        e.title = title
        e.url = "https://www.investing.com/indices/investing.com-us-500-components"
        self.bot.logger.info(e.url)

        return e

    async def get_data_embed_for_event(self, previous_released,
                                       event: pyinvesting.Event):
        e = self.get_embed()
        e.title = event.name
        e.url = "https://www.investing.com" + event.url
        self.bot.logger.info(e.url)

        e.add_field(name="Actual", value=event.actual)
        e.add_field(name="Expected", value=event.expected)
        e.add_field(name="Previous", value=event.previous)
        if previous_released != event.previous:
            msg = ('previous release was revised from {0}'.format(
                previous_released))
            e.set_footer(text=msg)
        return e

    async def on_message(self, message):
        if message.author == self.bot.user:
            return

        cmd_prefix = self.bot.command_prefix
        m = re.findall(r"฿(\D\S*)", message.content)
        if m:
            requests = ','.join(m)
            message.content = '%(prefix)squote %(requests)s' % {
                'requests': requests,
                'prefix': cmd_prefix
            }
            await self.bot.process_commands(message)

    def cool(self, symbol):
        curr_time = time.time()
        if (symbol not in self.symbol_cooldown.keys()) or (
                self.symbol_cooldown[symbol] <
                curr_time - self.symbol_cooldown_length):
            self.symbol_cooldown[symbol] = curr_time
            return True
        else:
            return False

    @commands.cooldown(1, 5, commands.BucketType.user)
    @commands.cooldown(3, 10, commands.BucketType.channel)
    @commands.command(
        pass_context=True,
        help="""
                Short Quote: [exchange:symbol] or [symbol]
                Extended Info: [exchange:symbol]+ or [symbol]+
            """)
    async def quote(self, ctx, symbol):
        self.bot.logger.info("Quoting %(symbol)s for %(user)s " % {
            'symbol': symbol,
            'user': ctx.message.author
        })

        quote_hash = hashlib.sha256(
            ("%s%s" % (symbol, ctx.message.channel.name)).encode('utf-8'))

        if not self.cool(quote_hash.hexdigest()):
            self.bot.logger.warning(
                'Skipping symbol %(symbol)s for cooldown' % {'symbol': symbol})
            return

        await ctx.bot.type()
        symbols = re.findall(r'([a-zA-Z][\w/:]*)\+?', symbol)

        if len(symbols) == 1 and symbol.endswith('+'):
            include_overview = True

        # Double + means add chart
        else:
            include_overview = False

        quote_start_time = time.time()
        embed = await self.get_data_embed_for_symbols(symbols, overview=include_overview)
        quote_time = time.time() - quote_start_time
        self.bot.logger.info(
            "Quoted %(symbol)s for %(user)s in %(seconds)s" % {
                'symbol': symbol,
                'user': ctx.message.author,
                'seconds': quote_time,
            })

        if embed:
            await ctx.bot.say(embed=embed, delete_after=300)

    @commands.cooldown(1, 30, commands.BucketType.user)
    @commands.cooldown(3, 30, commands.BucketType.channel)
    @commands.command(
        pass_context=True,
        help="""
                Chart a symbol: [exchange:symbol] or [symbol].  
                Supported resolutions: 1,5,15,30,60,240,d,w,m
            """)
    async def chart(self, ctx, symbol, resolution='15'):
        self.bot.logger.info("Charting %(symbol)s for %(user)s " % {
            'symbol': symbol,
            'user': ctx.message.author
        })

        resolutions = ['1', '5', '15', '30', '60', '240', 'd', 'w', 'm']

        if self.gyazo == None:
            self.bot.logger.error(
                'No access token configured.  Not attempting to chart')
            return

        await ctx.bot.type()

        if resolution not in resolutions:
            await ctx.bot.say("Supported resolutions: %s" % ','.join(resolutions), delete_after=30)
            return

        embed = await self.get_data_embed_for_symbols([symbol], overview=True)
        try:
            chart_buffer = await self.get_chart(
                symbol, resolution=resolution.upper())
            chart_url = await self.event_loop.run_in_executor(
                None, self.upload_chart, chart_buffer.getbuffer())
            self.bot.logger.info("Got chart url %s" % chart_url + " type is " +
                                 str(type(chart_url)))
            embed.set_image(url=chart_url)
        except Exception as e:
            self.bot.logger.exception(
                "Error generating message for %s: %s" % (symbol, e))
            embed.add_field(name="Error", value="Error fetching data")
        if embed:
            await ctx.bot.say(embed=embed, delete_after=120)

    @asyncio.coroutine
    async def get_bonds_table_data(self, iso2_country) -> Tuple[Optional[io.StringIO],
                                                                Optional[str],
                                                                Optional[str]]:
        bonds: List[pyinvesting.Bond] = self.investing.get_bonds(region=iso2_country)
        if bonds is None:
            return None, None, None

        self.bot.logger.info("bonds fetched with len " + str(len(bonds)))
        bond_yields_by_time_label: Dict[str, float] = (seq(bonds)
                                                       .map(
            lambda b: (b.duration_label, "{0:+.2f}".format(b.yield_pct)))
                                                       .to_dict())

        col1 = list(bond_yields_by_time_label.keys())
        col2 = list(bond_yields_by_time_label.values())
        column_names = ['Mat', 'Yield']
        table = io.StringIO()
        table.write("```")
        astropy.io.ascii.write(
            [col1, col2],
            table,
            format='fixed_width_two_line',
            formats={'Mat': '%-5s', 'Yield': '%-7s'},
            names=column_names)
        table.write("```")
        return table, bonds[0].country, bonds[0].url

    @commands.cooldown(1, 30, commands.BucketType.user)
    @commands.cooldown(3, 30, commands.BucketType.channel)
    @commands.command(
        pass_context=True,
        help="""
               Print government bond yield for an [region]
               """)
    async def bonds(self, ctx, region):
        self.bot.logger.info(
            "Getting term structure for {0}' govt. bonds for '{1}'".format(
                region,
                ctx.message.author,
            ))
        bond_hash = hashlib.sha256(
            ("bonds'{0}''{1}'".format(region, ctx.message.channel.name)).encode('utf-8'))

        if not self.cool(bond_hash.hexdigest()):
            self.bot.logger.warning(
                "Skipping bond '{0}' for cooldown".format(region))
            return

        try:
            await ctx.bot.type()

            (yield_table, country, url) = await self.get_bonds_table_data(region)

            if yield_table is None:
                return

            embed = await self.get_data_embed_for_bonds(country, url)
            embed.add_field(
                name="Bonds Term Structure",
                value=yield_table.getvalue(),
                inline=True)

            await ctx.bot.say(embed=embed, delete_after=60)

        except Exception as e:
            # noinspection PyUnboundLocalVariable
            self.bot.logger.exception(
                "Error generating message for '{0}' bonds: '{1}'".format(region, e))

    async def get_options_data(self, symbol, expiration):
        (full_chain, exp_epoch) = await self.event_loop.run_in_executor(None, symbol.get_options, expiration)

        if full_chain is None:
            return

        columns = ['call_bid', 'call_ask', 'strike', 'put_bid', 'put_ask']
        column_names = ['C/B', 'C/A', 'Strike', 'P/B', 'P/A']

        chain = full_chain[columns]
        values = chain.values
        table = io.StringIO()

        table.write("```")
        astropy.io.ascii.write(
            values,
            table,
            format='fixed_width_two_line',
            names=column_names,
            bookend=False)
        table.write("```")

        return table, exp_epoch

    @commands.cooldown(1, 30, commands.BucketType.user)
    @commands.cooldown(3, 30, commands.BucketType.channel)
    @commands.command(
        pass_context=True,
        help="""
            Print options chain for a symbol [exchange:symbol] or [symbol]
            [expiration] can be:
              Weeklies: fd1,fd2,fd3 
              or
              Monthlies: m0,m1,m2,m3
            """)
    async def options(self, ctx, symbol, expiration='m0'):
        self.bot.logger.info(
            'Getting options chain %(symbol)s for %(user)s' % {
                'symbol': symbol,
                'user': ctx.message.author,
            })

        option_hash = hashlib.sha256(
            ("options%s%s%s" % (symbol, ctx.message.channel.name,
                                expiration)).encode('utf-8'))

        if not self.cool(option_hash.hexdigest()):
            self.bot.logger.warning(
                'Skipping symbol %(symbol)s for cooldown' % {'symbol': symbol})
            return

        try:
            await ctx.bot.type()

            s = await self.event_loop.run_in_executor(None, self.investing.get_symbol, symbol)

            if s is None:
                return

            (options_table, exp_epoch) = await self.get_options_data(s, expiration)
            exp_date = datetime.datetime.fromtimestamp(exp_epoch).strftime(
                '%m/%d/%y')

            if options_table is None:
                return

            embed = await self.get_data_embed_for_symbols([symbol])
            embed.add_field(
                name='Options Chain - %(date)s' % {'date': exp_date},
                value=options_table.getvalue(),
                inline=False)
            await ctx.bot.say(embed=embed, delete_after=300)

        except Exception as e:
            self.bot.logger.exception(
                "Error generating message for %s: %s" % (symbol, e))

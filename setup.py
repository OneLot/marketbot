import setuptools  # pragma: no cover

setuptools.setup(
    name="marketbot",
    author="OxD",
    author_email="oxd@protonmail.com",
    description="All purpose market info bot",
    long_description='All purpose market info bot',
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GPLv3 License",
        "Operating System :: OS Independent",
    ),
    scripts=['bin/marketbot'],
    use_scm_version=True,
    setup_requires=['setuptools_scm'])  # pragma: no cover
